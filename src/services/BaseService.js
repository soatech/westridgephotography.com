'use strict';

// polyfills for fetch and promises
require('es6-promise').polyfill();
require('isomorphic-fetch');

import config from '../config/config';
import assign from 'object-assign';

const baseServiceFactory = function (spec) {
    const baseUrl = config.SERVER_BASE_URL;

    let baseOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };

    let baseReqBody = {
        j_username: config.USERNAME,
        j_password: config.PASSWORD
    };

    let BaseService = {
        buildUri: function buildUri(service, method) {
            return `${baseUrl}/${service}.cfc?method=${method}&j_username=${baseReqBody.j_username}&j_password=${baseReqBody.j_password}`;
        },
        buildOptions: function buildOptions(reqBody:Object, method:String) {
            let options = baseOptions;

            if (method) {
                options.method = method;
            }

            if (options.method !== 'GET') {
                options.body = JSON.stringify(assign({}, baseReqBody, reqBody));
            }

            return options;
        }
    };

    return BaseService;
};

export default baseServiceFactory;