'use strict';

import assign from 'object-assign';
import baseServiceFactory from './BaseService';

const galleryServiceFactory = function (spec) {
    const baseService = baseServiceFactory(spec);
    const serviceUrl = 'com/shinynet/galleries/services/GalleryService';

    return assign({}, baseService, {
        getActiveGalleries: function getActiveGalleries(success, failure) {

            let uri = this.buildUri(serviceUrl, 'getActiveGalleriesJSON');

            console.log(uri);

            fetch(uri, this.buildOptions())
                .then((response) => response.json())
                .then((responseData) => {
                    // check for error condition and send to failure
                    success(responseData);
                })
                .catch((error) => {
                    console.log(error);
                    failure(error);
                });
        }
    });
};

export default galleryServiceFactory;