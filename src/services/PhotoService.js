'use strict';

import assign from 'object-assign';
import baseServiceFactory from './BaseService';

const photoServiceFactory = function (spec) {
    const baseService = baseServiceFactory(spec);
    const serviceUrl = 'com/shinynet/galleries/services/PhotoService';

    return assign({}, baseService, {
        getPhotosForGallery: function getPhotosForGallery(galleryId, success, failure) {
            let uri = this.buildUri(serviceUrl, 'getPhotosForGalleryJSON');
            uri += `&galleryId=${galleryId}`;

            console.log(uri);

            fetch(uri, this.buildOptions())
                .then((response) => response.json())
                .then((responseData) => {
                    // check for error conditions
                    success(responseData);
                })
                .catch((error) => {
                    console.log(error);
                    failure(error);
                });
        }
    });
};

export default photoServiceFactory;