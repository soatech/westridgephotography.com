'use strict';

import React from 'react';
import {render} from 'react-dom';
import WPApp from './WPApp';

render(<WPApp/>, document.getElementById('wp-app'));
