'use strict';

import React from 'react';
import TopNav from './components/TopNav';
import Coverflow from './components/Coverflow';
import Contact from './components/Contact';
import assign from 'object-assign';

const WPApp = React.createClass({
    getInitialState() {
        return {
            activePage: 'gallery',
            activeGallery: 0
        };
    },
    _navChangeHandler(spec) {
        console.log(spec);

        let newState = {
            activePage: spec.page
        };

        if (spec.galleryId) {
            assign(newState, {
                activeGallery: spec.galleryId
            });
        }

        this.setState(newState);
    },
    render() {
        return <div>
            <TopNav navChangeHandler={this._navChangeHandler}/>,
            {this.state.activePage === 'contact' ? <Contact/> :
                <Coverflow galleryId={this.state.activeGallery}/>}
        </div>;
    }
});

export default WPApp;