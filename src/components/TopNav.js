'use strict';

import React from 'react';
import _ from 'lodash';
import galleryServiceFactory from '../services/GalleryService';

const GalleryService = galleryServiceFactory();

const TopNav = React.createClass({
    propTypes: {
        navChangeHandler: React.PropTypes.func.isRequired
    },
    getInitialState() {
        return {
            navItems: []
        };
    },
    componentDidMount() {
        let success = (data) => {
            // TODO: sort by "sortOrder" and filter by "active" using lodash
            this.setState({
                navItems: data
            }, function () {
                this.props.navChangeHandler({
                    page: 'gallery',
                    galleryId: this.state.navItems[0].galleryId
                });
            });
        };

        let failure = (error) => {
            console.log(error);
        };

        GalleryService.getActiveGalleries(success, failure);
    },
    render() {
        return <nav className="navbar navbar-default navbar-fixed-top navbar-inverse">
            <div className="container">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed"
                            data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <a className="navbar-brand" href="#">Westridge Photography</a>
                </div>
                <div id="navbar" className="navbar-collapse collapse">
                    <ul className="nav navbar-nav navbar-right">
                        {_.map(this.state.navItems, (item) => {
                            return <li key={`nav-${item.galleryId}`}>
                                <a href="#"
                                   onClick={(event) => this.props.navChangeHandler({page: 'gallery', galleryId: item.galleryId})}>{item.name}</a>
                            </li>;
                        })}
                        <li><a href="#" onClick={(event) => this.props.navChangeHandler({page: 'contact'})}>Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>;
    }
});

export default TopNav;