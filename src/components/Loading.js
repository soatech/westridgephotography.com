'use strict';

import React from 'react';

const Loading = React.createClass({
    render() {
        return <div className="container loading">
            Loading...
        </div>;
    }
});

export default Loading;