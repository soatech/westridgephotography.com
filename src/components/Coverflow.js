'use strict';

import React from 'react';
import _ from 'lodash';
import ReactCoverflow from 'react-coverflow';
import Loading from './Loading';
import photoServiceFactory from '../services/PhotoService';
import config from '../config/config';

const PhotoService = photoServiceFactory();

const Coverflow = React.createClass({
    propTypes: {
        galleryId: React.PropTypes.number.isRequired
    },
    getInitialState() {
        return {
            loading: true,
            galleryId: this.props.galleryId,
            images: []
        };
    },
    componentWillReceiveProps(nextProps) {
        this._fetchImages(nextProps.galleryId);
    },
    componentDidMount() {
        this._fetchImages(this.props.galleryId);
    },
    _fetchImages(galleryId) {

        let success = (data) => {
            this.setState({
                loading: false,
                galleryId: galleryId,
                images: data
            });
        };

        let failure = (error) => {
            console.log(error);
        };

        PhotoService.getPhotosForGallery(galleryId, success, failure);
    },
    _determineLoading() {
        return this.state.loading || (this.state.galleryId !== this.props.galleryId);
    },
    render() {
        return (this._determineLoading() ? <Loading/> : <ReactCoverflow
            height={680}
            navigation={true}
            enableHeading={false}
            displayQuantityOfSide={2}>
            {_.map(this.state.images, (image) => {
                return <img key={image.photoId} src={`${config.SERVER_BASE_URL}/photos/${image.filename}`}/>;
            })}
        </ReactCoverflow>);
    }
});

export default Coverflow;