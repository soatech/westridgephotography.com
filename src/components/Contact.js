'use strict';

import React from 'react';

const Contact = React.createClass({
    render() {
        return <div className="contact container">
            <div className="company">Westridge Photography</div>
            <div className="address">2635 64th Ave, Greeley, CO 80634</div>
            <div className="phone">(970) 330-2291</div>
            <div className="email"><a href="mailto:info@westridgephotography.com">info@westridgephotography.com</a>
            </div>
        </div>
    }
});

export default Contact;