# Overview #

Coverflow PoCs (Proofs of Concept) in HTML/CSS.

### What libraries are we using? ###

* Bootstrap for the top navigation
* jQuery for pre-reqs and data calls
* CoverflowJS
* HammerJS for touch detection
* ReactJS for application setup

### Setup ###

1. First run npm install to get ReactJS and all other necessary components.
1. You'll need to modify the json files in data.json to pull in different images.  Then put those images in a folder structure like: img/{collection-name}/{image-file}.
1. Execute `npm start` to build and watch src/index.js
1. Execute `npm run server` to start a simple http server (or use IntelliJ's in index.html)
1. To change where your services point to, edit `config.js` and `env/(local|prod).js`